﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio2
{ /*Crear un programa que permita cargar los nombres de 5 empleados y sus salarios respectivos. 
    mostrar el salario mayor y el nombre del empleado. 
    
    Crear un metodo para cargar. Cargar()
    Crear un metodo para calcular el salario mayor. SalarioMayor()
    
    Nota:
    Al finalizar el mensaje debe salir de la siguiente forma.
    
    Console.WriteLine("El empleado con salario mayor es: {0}", nombres[pos]);
    Console.WriteLine("Tiene un salario de: {0}",mayor);*/


    class ClaseA
    {
        private string[] nombres;
        private float[] SALARIO;
        public void Cargar()
        {
            nombres = new string[5];
            SALARIO = new float[5];


            Console.WriteLine("\n        ////////////////////////////////////////////////////////////////////////////////");
            Console.WriteLine("       //       Cordial saludo, este programa permite calcular por usted,            //");
            Console.WriteLine("      //   el sueldo mas elevado de los empleados segun los datos que se ingresen.  //");
            Console.WriteLine("     ////////////////////////////////////////////////////////////////////////////////");

            for (int i = 0; i < nombres.Length; i++)
            {
                Console.Write("\nFavor de ingresar el nombre del empleado " + (i + 1) + ": ");
                nombres[i] = Console.ReadLine();
                Console.Write("Favor de ingresar el salario del empleado " + (i + 1) + ": ");

                string linea;
                linea = Console.ReadLine();
                SALARIO[i] = float.Parse(linea);
            }
        }
        public void SalarioMayor()
        {
            float mayor;
            int pos;
            mayor = SALARIO[0];
            pos = 0;




            for (int I = 1; I < nombres.Length; I++)
            {
                if (SALARIO[I] > mayor)
                {
                    mayor = SALARIO[I];
                    pos = I;
                }
            }
            Console.WriteLine("\n______________________________________________________");


            Console.WriteLine("El empleado con salario mayor es {0}", nombres[pos]);
            Console.WriteLine("Tiene un salario de: {0}", mayor);


            Console.WriteLine("\nGRACIAS POR PREFERIRNOS!!");

            Console.ReadKey();
        }



        static void Main(string[] args)
        {
            ClaseA pv = new ClaseA();
            pv.Cargar();
            pv.SalarioMayor();
        }
    }
}
